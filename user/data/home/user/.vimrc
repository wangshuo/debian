" ~/.vimrc

syntax on
set secure
set modeline
set background=dark
set showcmd
set showmatch
set incsearch
set hlsearch
set list
set listchars=tab:>-,trail:-,nbsp:%
set wildmode=longest,list,full

set fileencodings=ucs-bom,utf-8,gb18030,latin1
