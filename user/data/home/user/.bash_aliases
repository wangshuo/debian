# ~/.bash_aliases

HISTCONTROL=ignoreboth:erasedups
alias less='less -R'
PS1='\[\033[01;31m\]?$?\[\033[01;33m\]L$SHLVL[\j]\[\033[00m\]'"$PS1"
alias colorls='ls --color=always'
alias colorgrep='grep --color=always'
alias diff='diff --color=auto'
alias colordiff='diff --color=always'
alias diffstat='diffstat -C'
alias tree='tree -C'
alias l.='ls -AI\*'

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ln='ln -i'

export EDITOR=vim
alias man='man -a'
alias type='type -a'
alias command-not-found='command-not-found --ignore-installed'
[[ "$PATH" =~ /sbin ]] || PATH="$PATH:/usr/local/sbin:/usr/sbin:/sbin"
[[ "$PATH" =~ ~/bin ]] || PATH="$PATH:$HOME/bin"
